---
title: "Configura la ventana de entrada de tu Koala"
date: 2010-01-24 11:25
categories: ["Linux"]
tags: ["Linux", "Informática", "Karmic", "Ubuntu"]
---

#### &nbsp; ####

Desde que apareció la versión Karmic Koala de Ubuntu uno de los mayores quebraderos de cabeza parece ser que era cambiar la pantalla de entrada al escritorio. El fondo de color chocolate, nos recordaba la escena de la popular serie de humor Mr. Bean (Mr. ZP) cuando cae al suelo desde los cielos.

[![Pantalla GDM2 en Karmic koala][1]][2]

Acabo de encontrarme un proyecto en launchpad que nos facilita la tarea de cambiar esta pantalla fácilmente. La dirección es:

[**https://launchpad.net/gdm2setup**][3]

Desde allí podemos añadir el repositorio con las instrucciones que nos indican, y después unicamente tendremos que instalar el paquete gdm2setup.

``` bash
    sudo add-apt-repository ppa:gdm2setup/gdm2setup
    sudo aptitude update
    sudo aptitude install python-gdm2setup
```

Espero que os sirva. Un saludo

[1]: http://valhue.gitlab.io/imagenes/2010/01/bg_2560x1600-300x187.jpg "Pantalla GDM2 en Karmic koala"
[2]: http://valhue.gitlab.io/imagenes/2010/01/bg_2560x1600.jpg "Pantalla GDM2 en Karmic koala"
[3]: https://launchpad.net/gdm2setup "GDM2Setup in Launchpad"
