---
title: "Mi escritorio"
date: "2013-02-23T10:52:34+01:00"
categories: ["Linux"]
tags: ["Alsi", "ArchLinux", "Cinnamon", "Linux"]
---

En esta captura se puede apreciar cual ha sido mi elección final de DM, iconos, tema GTk, fuente, etc...

[![][1]][2]

Para mostrar la información del sistema en la terminal he recurrido a [alsi][3], disponible en AUR. Para que aparezca Cinnamon como el escritorio utilizado he editado el archivo __~/.config/alsi/alsi.de__ y como cinnamon es detectado como "gnome-session", dejamos la línea donde aparece de este modo:<!-- more -->
``` bash
	gnome-session => CINNAMON,
```
Si queremos que alsi se ejecute en cada inicio de sesión tendremos que añadir la línea siguiente:
``` bash
	# Hacemos la terminal más amigable
	alsi
```
hacia el final del archivo **~/.bashrc**.

Espero que os sea útil.


[1]: https://valhue.gitlab.io/imagenes/2013/02/Mi_terminal-300x163.png "Mi terminal"
[2]: https://valhue.gitlab.io/imagenes/2013/02/Mi_terminal.png
[3]: https://aur.archlinux.org/packages/alsi/ "alsi AUR package"
