---
title: "Gufw: Violación de segmento ('core' generado)"
date: "2015-03-08T12:49:00+01:00"
tags: ["ArchLinux", "Gnome 3.14", "firewall", "ufw", "gufw"]
categories: ["Linux"]
---

Estaba el otro día instalando un [***firewall sencillito***](https://wiki.archlinux.org/index.php/Uncomplicated_Firewall), y como después de configurar unas cuantas reglas y activarlo la persona que tendria que utilizar ese PC "*no se lleva muy bien con la linea de comandos*" decidí instalarle [***gufw***](http://gufw.org/), una interfaz gráfica para [***ufw***](https://launchpad.net/ufw).


[![](https://valhue.gitlab.io/imagenes/2015/03/gufw.png)](https://valhue.gitlab.io/imagenes/2015/03/gufw.png)

<!-- more -->

El problema viene ahora, después de realizar una instalación exitosa con:

``` bash
	sudo pacman -S gufw
```
intento lanzar la aplicación y nada, no aparece nada. **¿Que está pasando?**   &nbsp; :( &nbsp;

Abro una terminal y esta vez lanzo la aplicación desde la linea de comandos para observar algún error. El error se manifiesta. "***Violación de segmento ('core' generado)***"

Investigo el tema y me encuentro que si lanzo gufw con:
``` bash
	sudo python2 /usr/lib/python2.7/site-packages/gufw/gufw.py
```
entonces gufw si funciona y se muestra en nuestro escritorio. Miro dentro de /usr/share/applications/gufw.desktop
``` bash
	cat /usr/share/applications/gufw.desktop
```
y me encuentro con
``` bash
	Exec=gufw
```
por lo que miro en /usr/bin/gufw
``` bash
	cat /usr/bin/gufw
```
y me encuentro
``` bash
	#!/bin/sh
	c_user=$(whoami)
	pkexec gufw-pkexec $c_user
```
Aparentemente todo es correcto, pero si lanzando manualmente gufw, como explico un poquito arriba, este funciona ¿porque no lo hace con */usr/bin/gufw*?

Pensé en poner la ruta completa a *gufw-pkexec*, porque creo que siempre que uso pkexec lo hago así. Primero hago..
``` bash
	sudo -i
```
y ahora edito */usr/bin/gufw*
``` bash
	vim /usr/bin/gufw
```
y dejo del siguiente modo su contenido
``` bash
	#!/bin/sh
	c_user=$(whoami)
	pkexec /usr/bin/gufw-pkexec $c_user
```
Intento de nuevo ejecutar gufw, "**et voilà**", **FUNCIONA**.
Ya lo sabéis, espero que os haya sido util, y...


Que lo disfrutéis. &nbsp; ;) &nbsp;

