---
title: "Flash Player compatible con Nuvola Player"
date: "2014-09-25T20:39:13+01:00"
categories: ["Linux"]
tags: ["ArchLinux", "Nuvola Player", "Flash Player"]
---

Nuvola Player es un reproductor que integra en él toda la música de la nube en su propia ventana y proporciona integración con el escritorio Linux (teclas multimedia, bandeja del sistema, los applets del reproductor multimedia, menú dock, notificaciones y más).

[![](https://valhue.gitlab.io/imagenes/2014/09/nuvolaplayer.png)](https://tiliado.eu/static/nuvolaplayer/gnome_shell_all.png)

Pero Nuvola Player viene un ***problemilla*** desde hace mucho tiempo y es su compatibilidad con el plugin oficial de Flash de Adobe.

Con esta entrada os diré como solucionarlo, está extraido de la propia web oficial de Nuvola, pero así lo teneis más a mano y resumido. &nbsp; :D &nbsp;<!-- more -->

### El problemilla ###

Muchos servicios de streaming basados en la Web utilizan Adobe Flash para la reproducción de música. Desafortunadamente, el plugin de Adobe Flash no es compatible con Nuvola Player debido a las bibliotecas en conflicto (GTK2 y GTK3). Es necesario nuestra intervención para solucionar este problema. Este tutorial describe cómo instalar el plugin de flash compatible en Arch Linux.

El paquete Nuvola Player en AUR no instala el plugin de Flash automáticamente, así que teneis que hacerlo por vosotros mismos.

## Tutorial ##

1. Instalar las dependencias necesarias en Flash mediante la ejecución de uno de los siguientes comandos:
``` bash
    pacman -S flashplugin;        # En los sitemas de 32-bits
    pacman -S lib32-flashplugin;  # En los sitemas de 64-bits
```
2. Teneis que instalar también lib32-libpulse y lib32-alsa-plugins en sistemas de 64 bits.
``` bash
    pacman -S lib32-libpulse lib32-alsa-plugins;  # En los sistemas de 64-bits
```
3. Instale nspluginwrapper:
``` bash
    pacman -S nspluginwrapper
```
4. Descargar el script [nuvolaplayer-flashplugin-installer](http://bazaar.launchpad.net/~fenryxo/nuvola-player/flashplugin/download/head:/nuvolaplayerflashplu-20130706115431-02vbepty4pijbi0y-3/nuvolaplayer-flashplugin-installer)

5. Ejecuta el script para instalar el plugin de Flash en el wrapper.
``` bash
    sudo bash ./nuvolaplayer-flashplugin-installer --install --force
```
6. Reinicie Nuvola Player.

Esto es todo, que lo disfruteis. &nbsp; ;) &nbsp;
