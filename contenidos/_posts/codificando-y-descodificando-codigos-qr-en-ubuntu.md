---
title: "Codificando y Descodificando codigos QR en Ubuntu"
date: "2011-02-19T13:00:24+01:00"
categories: ["Linux"]
tags: ["Linux", "QRCode", "Seguridad", "Ubuntu"]
---

#### &nbsp; ####

A continuación explicaré como integrar una aplicación de códigos QR en nuestro Ubuntu. Como viene siendo habitual en mí, describiré los pasos para instalarla en modo usuario, si por el contrario queréis hacerlo para todos los usuarios de vuestro sistema, unicamente tenéis que cambiar las rutas donde creo los archivos, y en lugar de hacerlo en *~/.local/share*, deberéis hacerlo en */usr/share*.

También agradecer al autor de esta aplicación, que aunque el dice que está verde, funciona perfectamente, y supongo que pronto la mejorará (le vengo siguiendo hace tiempo, y es un magnifico creador de aplicaciones linux). La fuente de todo esto está [**aquí**][1].

-- **Bien**, manos a la obra:

En primer lugar nos descargaremos la aplicación desde [***el blog del autor***][2], o desde [***este***][3] enlace.
``` bash
    wget http://www.atareao.es/downloads/varios/QRCode.0.1.0.zip -O ~/.local/lib/QRCode.zip
```
<!-- more -->
Lo extraemos en ~/.local/lib con:
``` bash
    unzip ~/.local/lib/QRCode.zip
    rm ~/.local/lib/QRCode.zip
```
A continuación creamos un script que se encargará de ejecutar la aplicación, para lo cual hacemos:
``` bash
    touch ~/.local/bin/QRCode
    nano ~/.local/bin/QRCode
```
y pondremos en su interior:
``` bash
    !/bin/sh

    ## Script by ValHue``**

    exec java -jar /$HOME/.local/lib/QRCode.jar

    fi
```
Control+O para guardar los cambios, y con Control+X cerramos el editor.

Para hacerlo ejecutable:
``` bash
    chmod +x ~/.local/bin/QRCode
```
Ahora podremos lanzar la aplicación desde cualquier ruta con solo ejecutar ***QRCode***. Para rematar la faena, crearemos un lanzador que se integre en el menú de Ubuntu. Guardamos la imagen siguiente en ***~/.local/share/icons/QRCode.png***

![][5]

Sólo nos queda crear el lanzador, para lo cual hacemos:
``` bash
    touch ~/.local/share/applications/QRCode.desktop
    nano ~/.local/share/applications/QRCode.desktop
```
Y en su interior colocamos:
``` bash
    [Desktop Entry]

    Encoding=UTF-8
    Name=QRCode
    Comment=QRCode es una pequeña aplicación que permite generar códigos QRCode y descodificarlos.
    Exec=QRCode
    Icon=QRCode
    Terminal=false
    Type=Application
    Categories=GNOME;GTK;Application;Graphics;VectorGraphics;Viewer;
    StartupNotify=true
```
Control+O para guardar los cambios, y con Control+X cerramos el editor.

Ya podremos lanzar la aplicación desde el menú ***Aplicaciones --> Gráficos --> QRCode***

***

*PD: Muchas Gracias a Lorenzo, su creador, aka "[**El atareao**][4]"*

El propio autor ha creado una versión gráfica escrita en python, a la que ha llamado gqrcode y que se puede instalar desde su repositorio:
``` bash
    sudo add-apt-repository ppa:atareao/atareao && sudo apt-get update
    sudo apt-get install gqrcode
```

[1]: http://www.atareao.es/ubuntu/software-para-tu-ubuntu/qrcode-y-ubuntu/ "QRCode y Ubuntu"
[2]: http://www.atareao.es/ubuntu/software-para-tu-ubuntu/qrcode-y-ubuntu/ "QRCode y Ubuntu"
[3]: http://www.atareao.es/downloads/varios/QRCode.0.1.0.zip "QRCode.0.1.0.zip"
[4]: http://www.atareao.es/ "El atareao"
[5]: https://valhue.gitlab.io/imagenes/2011/02/QRCode.png "Icono QRCode"
