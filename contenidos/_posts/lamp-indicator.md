---
title: "Instalación de un servidor LAMP (II), o..."
date: "2011-08-24T13:58:00+01:00"
categories: ["Linux"]
tags: ["Apache", "Linux", "Indicator", "Informática", "MySQL", "Perl", "Php", "Python", "Servidores", "Ubuntu"]
---

## ...Como crear un indicador para controlarlo. ##


Bien, esta entrada no es más que la continuación y/o evolución &nbsp; :D &nbsp; a la anterior [**Instalación de un servidor LAMP (I)**][1]

No soy ningún erudito de Linux, pero si que realizo algunas páginas web y llevo su mantenimiento, a parte de haber desarrollado algún mini-script para uso personal. Para mi es fundamental tener un servidor LAMP instalado. En la anterior entrada ya explicaba como lo podíamos conseguir y como lo tengo yo. Pero ahora que está de moda usar indicadores para casi todo me pregunté, ¿***podría crear un indicador que me ayudase a saber si tengo LAMP activo o apagado***?, o ¿***porque no activarlo y desactivarlo desde el indicador***?

Empecé a buscar información por ahí, en la nube, y encontré información de cómo crear un indicador para el área de indicadores de Ubuntu, pero dejé aparcado este proyecto por falta de tiempo y de "entendederas" hasta que esta semana, dado que me extrajeron una muela que llevaba desde Junio doliéndome lo que no está en los escritos :O , me he centrado otra vez en el tema y aquí lo que he conseguido.<!-- more -->

**A mi me sobra**, y creo que tardaré bastante en retomarlo para mejorarlo, pero **lo subo a la red por si a alguien le sirve** (***ya sea como a mi, tal cual, o para inspirarse y mejorarlo, quien sabe...***).

Todavía no se como crear un paquete deb, no le he dedicado el tiempo suficiente, por eso explicaré aquí como lo he creado y como lo tengo instalado. Os recuerdo una vez más que utilizo el directorio ~/.local para los archivos "***share***", "***lib***" y "***bin***" del usuario.

### -- ¡¡ Al turrón !! ###

He creado unos iconos para el indicador. Estos necesitan guardarse en ~/.local/share/icons si realizamos la instalación a nivel de nuestro usuario, o en /usr/share/icons si la realizamos para todos los usuarios. Las imágenes usadas como iconos son:

[![][2]][2] </br> [![][3]][3] </br> [![][4]][4]

y las podéis descargar de [**aquí**][5].

Si bajáis el archivo comprimido tendréis que hacer:
``` bash
    cd ~/.local/share/icons
    wget https://valhue.gitlab.io/descargas/2011/08/lamp-icons.tar.gz
    tar xzvf lamp-icons.tar.gz
    rm lamp-icons.tar.gz
```
Ya los tenemos en el lugar adecuado.

El archivo ejecutable lo pondré en ~/.local/bin por lo que haré:
``` bash
    cd ~/.local/bin
    wget https://valhue.gitlab.io/descargas/2011/08/lamp-indicator.tar.gz
    tar xzvf lamp-indicator.tar.gz
    rm lamp-indicator.tar.gz
```
Ahora colocamos una entrada en el menú de gnome:
``` bash
    cd ~/.local/share/applications
    wget https://valhue.gitlab.io/descargas/2011/08/lamp-indicator.desktop.tar.gz
    tar xzvf lamp-indicator.desktop.tar.gz
    rm lamp-indicator.desktop.tar.gz
```
Y si queremos que se inicie en cada sesion:
``` bash
    cd ~/.config/autostart
    wget https://valhue.gitlab.io/descargas/2011/08/lamp-indicator.autostart.tar.gz
    tar xzvf lamp-indicator.autostart.tar.gz
    rm lamp-indicator.autostart.tar.gz
```
**AVISO**:

_Como el servidor apache y mysql se iniciaban de modo automático en cada reinicio, al para el servidor LAMP les cambio los permisos de modo que si no vamos a necesitar LAMP en el siguiente reinicio pues nos ahorramos el consumo de recursos de estos "servicios". Para todo esto llamo varias veces a "gksu" que actualmente en Ubuntu no cachea las contraseñas, pero si como YO, le pones a root una contraseña (no se le asigna por defecto en Ubuntu) puedes hacer mediante gksu-properties que la guarde por el periodo que tu quieras. ¡¡OJO, que ahora te pide la contraseña de Root!! no de tu usuario._

--_Esto lo podéis cambiar editando el archivo ejecutable instalado en ~/.local/bin y cambiando todos los gksu por gksudo._

[![][6]][6]

Un saludo, y espero que os sirva...

&nbsp; :D &nbsp;

[1]: https://valhue.gitlab.io/entradas/2010/09/29/instalacion-de-un-servidor-lamp/ "Instalación de un servidor LAMP (I)"
[2]: https://valhue.gitlab.io/imagenes/2011/08/lamp.png "Lamp"
[3]: https://valhue.gitlab.io/imagenes/2011/08/lampon.png "Lamp ON"
[4]: https://valhue.gitlab.io/imagenes/2011/08/lampoff.png "Lamp OFF"
[5]: https://valhue.gitlab.io/descargas/2011/08/lamp-icons.tar.gz "lamp-icons.tar.gz"
[6]: https://valhue.gitlab.io/imagenes/2011/08/Privilege-granting-preferences_001.png "Privilege granting preferences"
