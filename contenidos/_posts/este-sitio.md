---
title: "Sí, este blog está alimentado por Pelican"
date: "2013-07-27T11:52:34+01:00"
categories : ["El Blog"]
tags: ["El Blog", "Web"]
---

Era inevitable, aunque me ha costado mucho tomar la decisión, pero al final dejo WordPress y me decanto por lo simple y lo sencillo, y además me ofrece un nivel superior de seguridad.

[![][1]][2]

El sitio empezaba a notarse lento, y aunque miré otros proveedores, e intente reducir al máximo el numero de peticiones http, notaba que le costaba, y eso que todavía no tenia muy saturada la base de datos. Me planteé utilizar paginas estáticas en el sitio, pero a medida que crecieran las entradas sería un trabajo enorme mantener los enlaces entre ellas, etc... **¿Existian gestores de sitios web que generasen paginas estáticas?**.<!-- more -->

Pues si. Realicé una búsqueda y me encontré con [**Pelican**][3], [**Acrylamid**][4], [**Nikola**][5], [**Tinkerer**][6], etc...

Lo mejor de esta campaña en la cual me he metido voluntariamente, es que las entradas son archivos de texto plano, en mi caso markdown, y en cualquier momento puedo cambiar de un gestor a otro, pues todos los que he citado manejan [**Markdown**][7].

Ahora mismo estoy en fase de pruebas y ajustes, pero que no cunda en pánico ni nadie se asuste, he realizado copia de los contenidos del antiguo blog y cuando tenga Pelican ajustado lo recuperaré todo.

Ser pacientes...

[1]: https://valhue.gitlab.io/imagenes/2013/07/pelican.png "Pelican"
[2]: http://docs.getpelican.com/en/3.2/ "Documentación Pelican"
[3]: http://blog.getpelican.com/ "Blog de Pelican"
[4]: http://posativ.org/acrylamid/ "Acrylamid"
[5]: http://nikola.ralsina.com.ar/ "Nikola"
[6]: http://tinkerer.me/ "Tinkerer"
[7]: http://daringfireball.net/projects/markdown/ "Markdown"
