---
title: "Instalando JDownLoader sólo para nuestro usuario"
date: "2010-04-22 19:10"
categories: ["Linux"]
tags: ["Linux", "Informática", "Karmic", "Lucid", "Maverick", "Ubuntu"]
---

## &nbsp; ##

![image0][1]

Esta entrada esta basada en la [***anterior***][2], donde explicaba como aprovecharnos del directorio ~/.local para ubicar los ejecutables de nuestro usuario, entre otras cosas.<!-- more -->

JDownLoader es una plataforma de código abierto escrita completamente en Java, diseñado para simplificar la descarga de archivos de servidores como Rapidshare.com o Megaupload.com, no solo para usuarios con cuenta Premium sino también para los de cuenta gratuita.

JD ofrece descargas múltiples paralelas, reconocimiento de captcha, extracción automática de archivos, administración de contraseñas y mucho más. Adicionalmente, soporta muchos sitios de "encriptación de enlaces", así que solo tendrá que pegar los enlaces "encriptados" y JD hará el resto. JD puede importar archivos DLC, CCF y RSDF.

Por supuesto, JD es gratuito.

Para que nos funcione sin problemas necesitaremos el "entorno en tiempo de ejecución" de Java 1.5 o posterior (JRE o Java Runtime Environment), pero con OpenJDK también funciona en la ultima versión.

Aunque en la web de JDownLoader se explica un método mas sencillo para su instalación, aquí explicaré como lo hice yo para aprovecharme precisamente de la estructura de directorios explicada con anterioridad.

###PROCEDAMOS:###

``` bash
    mkdir ~/.local/lib/JDownloader
    cd ~/.local/lib/JDownloader
    wget http://update0.jdownloader.org/jdupdate.jar
    java -Xmx512m -jar jdupdate.jar
```

Con esto habremos instalado en ~/.local/lib/JDownloader la última versión disponible del programa. A continuación se ejecutará de forma automática el programa de instalación, al cuál le tendremos que ir dando una serie de datos para completar la instalación correctamente. Elegimos el directorio por defecto de las descargas, aunque esto se podrá modificar después desde las opciones del propio programa, y nos preguntará si queremos integrar JDownloader a Firefox usando FlashGot, yo lo tengo instalado.

Una vez terminado este proceso ya lo tendremos funcionando y actualizado a la última versión. Primero creamos el ejecutable para lanzarlo desde donde queramos y a continuación crearemos la entrada para el menú de Inicio.

``` bash
    touch ~/.local/bin/jdownloader
    nano ~/.local/bin/jdownloader
```

Escribimos en su interior:

``` bash
    #!/bin/sh

    java -Xmx512m -jar ~/.local/lib/JDownloader/JDownloader.jar
```

Lo guardamos con <Control>+O y cerramos nano con <Control>+X. Lo hacemos ejecutable con:

``` bash
    chmod +x ~/.local/bin/jdownloader
```

Ahora probaremos que el ejecutable creado funciona, para ello tecleamos en la terminal jdownloader+<Enter>

Si ya tenias el directorio ~/.local/bin activado, según expliqué en otra entrada, te deberá haber funcionado sin el mayor problema. Solamente nos queda crear una entrada en nuestro Menú de Inicio, para poderlo abrir en modo gráfico sin necesidad de teclear nada en la terminal.

``` bash
    ln -s ~/.local/lib/JDownloader/jd/img/logo/jd_logo_128_128.png ~/.local/share/icons/jd_logo.png
    touch ~/.local/share/applications/jdownloader.desktop
    nano ~/.local/share/applications/jdownloader.desktop
```

Escribimos en su interior:

``` bash
    #!/usr/bin/env xdg-open

    [Desktop Entry]
    Version=1.0
    Encoding=UTF-8
    Type=Application
    Name=JDownLoader
    Comment=Download manager for one-click hosting sites
    Categories=Application;Network;FileTransfer;
    Exec=/bin/sh "/home/TU_USUARIO/.local/bin/jdownloader"
    Icon=jd_logo.png
    Terminal=false
    StartupNotify=false
```

Lo cerramos al igual que con el anterior y ya tendremos en **Menú->Aplicaciones->Internet** el icono correspondiente al programa.

Espero que os haya gustado y podáis aprovecharlo. Saludos.

** ;) **

[1]: https://valhue.gitlab.io/imagenes/2010/04/jdownloader-300x300.png "JDownloader"
[2]: https://valhue.gitlab.io/entradas/2010/04/06/aprende-a-utilizar-el-directorio-local/ "Aprende a utilizar el directorio ~/.local"
