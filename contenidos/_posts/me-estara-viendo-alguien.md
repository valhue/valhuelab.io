---
title: "¿Me estará viendo alguien?"
date: "2011-03-24T10:35:22+01:00"
categories: ["Linux"]
tags: ["Linux", "Informática", "Seguridad", "Ubuntu", "WebCam"]
---

#### &nbsp; ####

[![][1]][2]

## **Cámara Monitor**

¿Está tu webcam encendida?


**Cámara Monitor** es un icono de la bandeja de sistema que te avisa cuando está encendida tu webcam.

Está diseñado para el escritorio [GNOME][3], pero funciona también en [KDE][4] y [XFCE][5]. Cámara Monitor está liberado bajo los términos de la [GNU General Public License][6].

Para instalarlo sólo tendremos que ejecutar en una terminal:<!-- more -->

``` bash
    sudo apt-get install cameramonitor
```

y una vez instalado...


**-- ¿Cómo puedo ejecutarlo?**

Lanzamiento de cameramonitor desde la línea de comandos: ***cameramonitor***

O desde el menú de GNOME: ***Aplicaciones --> Sonido y vídeo --> Monitor de la cámara***

Para cambiar las preferencias desde la línea de comandos: ***cameramonitor -p***

O desde el menú de GNOME: ***Preferencias --> Camera Monitor Preferences***


He observado que a veces cuando se inicia sesión, esta pequeña aplicación da un error, y no se carga en el inicio, pues piensa que ya está funcionando. Al parecer no se elimina el archivo *.pid que le indica que ya está funcionando, al cerrar sesión.

La solución a este pequeño "bug" es muy simple. Abrimos la terminal y ejecutamos:

``` bash
    sudo gedit /etc/rc.local
```

y le añadimos justo encima de la línea donde pone **exit 0**

``` bash
    #Borramos el pid de Camera Monitor
    rm /$HOME/.cameramonitor.pid
```

Ahora, en cada reinicio se borrará el archivo *.pid y cameramonitor se iniciará sin mensajes de error.


¡¡ Disfrutadlo !!


[1]: https://valhue.gitlab.io/imagenes/2011/03/cameramonitor.png "Camera Monitor"
[2]: http://www.infinicode.org/code/cameramonitor/
[3]: http://translate.googleusercontent.com/translate_c?hl=es&ie=UTF-8&langpair=auto%7Ces&u=http://www.gnome.org/&tbb=1&rurl=translate.google.com&usg=ALkJrhikPQ8H7M_rwSkJ2wIHF983ZhfR4A "GNOME"
[4]: http://translate.googleusercontent.com/translate_c?hl=es&ie=UTF-8&langpair=auto%7Ces&u=http://www.kde.org/&tbb=1&rurl=translate.google.com&usg=ALkJrhgHfOshEn8kLxnHpbjw8jkoeM32fg "KDE"
[5]: http://translate.googleusercontent.com/translate_c?hl=es&ie=UTF-8&langpair=auto%7Ces&u=http://www.xfce.org/&tbb=1&rurl=translate.google.com&usg=ALkJrhgFGQrCzUz-9Y9f0L35VLza_FtlsA "XFCE"
[6]: http://translate.googleusercontent.com/translate_c?hl=es&ie=UTF-8&langpair=auto%7Ces&u=http://www.gnu.org/licenses/gpl.txt&tbb=1&rurl=translate.google.com&usg=ALkJrhgD_DYhom8e8C12HWcZtcNOJWS2AQ "GNU General Public License"
