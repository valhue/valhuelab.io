---
title: "HUAWEI 220 funcionando en Karmic"
date: "2009-11-06T12:56:18+01:00"
categories: ["Linux"]
tags: ["E220", "GNU/Linux", "Huawei", "Karmic", "Ubuntu"]
---

## &nbsp; ##

Hasta que salió Karmic, este modem 3G requería de cierta dosis de paciencia y de bastante trabajo de configuración. Estos modems 3G llevan a su vez una unidad de almacenamiento incorporada, donde el fabricante o la operadora, alojan el software y los drivers necesarios, para hacerles funcionar en Windows.

En anteriores versiones de Ubuntu, esta unidad externa provocaba más de un dolor de cabeza, y antes de lanzar cualquier aplicación para conectarse a la red 3G debíamos desmontarla e incluso remover el driver con:
``` bash
    sudo rmmod usb_storage
```
Ahora, con karmic es de lo más sencillo. Únicamente deberemos crear "el driver" del modem de la siguiente manera, y una vez hecho, da igual si se tiene montada la unidad de almacenamiento interna, si se tiene cargado el modulo usb\_storage, o si la abuela fuma, porque en karmic funciona a las mil maravillas y el modem conecta a la primera sin ningún problema con el proveedor de 3G.
``` bash
    sudo gedit /etc/modprobe.d/huawei220.conf
```
En su interior escribimos:
``` bash
    #Huawei E220 alias huawei usbserial options huawei vendor=0x12d1 product=0x1003
```

![][1]

Lo guardamos y reiniciamos. Ya tenemos el sistema listo para utilizar nuestro modem 3G.

Yo en mi caso utilizo UMTSmon para conectarme, y mediante alien y utilizando de fuente el rpm disponible en Fedora me he creado mi propio deb, y aquí lo cuelgo para quien quiera utilizarlo.

[**UMTSmon empaquetado para Ubuntu**][2]

Un saludo.

[1]: https://valhue.gitlab.io/imagenes/2009/11/Pantallazo-UMTSmon.png "UMTSmon funcionando en mi portátil"
[2]: https://valhue.gitlab.io/descargas/2009/11/umtsmon_0.9.72.20090509-5.1_amd64.deb "UMTSmon empaquetado para Ubuntu"
