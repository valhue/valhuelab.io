---
title: Instalando autofirma en ArchLinux
date: 2019-03-24 17:09:49
categories: Linux
tags: 
  - autofirma
  - ArchLinux
  - java
  - certificado digital
---

El motivo de escribir esta entrada ha sido que después de horas, viendo como no podía firmar digitalmente en una web, 

- con un certificado digital que tengo instalado en mi navegador web, 
- y que no caduca hasta 2021, 
- que si funciona, porque me ha permitido identificarme en otras webs para realizar otros tramites e incluso para solicitar nuevas claves.. 

.. voy y descubro que en algunos sitios oficiales utilizan una aplicación llamada autofirma y aunque ya la tenía, en esta ocasión no me funcionaba, y los que me ha tenido más despistado ha sido que decía que no tenía ningún certificado instalado en mi depósito.

-- ***¿Cómo? "^`&%·#@"***

<!-- more -->
Así instalo autofirma en ArchLinux:

``` bash
$> yay -S autofirma
```
Y esto es lo que obtenía, intento tras intento....

![](https://valhue.gitlab.io/imagenes/2019/03/error-autofirma.png)

Investigando un poco por la red me encuentro con que [**autofirma**]( https://firmaelectronica.gob.es/Home/Descargas.html) funciona con java, siempre que su versión no sea superior a la 8. En ArchLinux, actualmente estamos utilizando la 11, con lo cual  mucho cuidado porque podrías entrar en bucle, como me ha pasado a mí, y pillar un soberano cabreo con los creadores de **autofirma**.

{% colorquote warning %}
Mucho cuidado con la versión de java que tengais instalada, autofirma sólo funciona correctamente con versiones de java inferiores o igual a la versión 8.
{% endcolorquote %}

Compruebo la versión de java que tengo activada por defecto con:

``` bash
$> archlinux-java status
```
y efectivamente me encuentro que tengo java 11

```
Available Java environments:
  java-11-openjdk (default)
  java-8-openjdk
```

Para solucionarlo, debes  ejecutar

``` bash
$> sudo archlinux-java set java-8-openjdk
```

Si no tienes la opción de seleccionar java 8 deberás instalartela en primer lugar, tecleando los siguientes comandos:

``` bash
$> sudo pacman -S jdk8-openjdk
```

No es necesario reiniciar. Podemos repetir el paso anterior para comprobar la versión activada por defecto y ahora tendremos java-8 según se observa

```
Available Java environments:
  java-11-openjdk
  java-8-openjdk (default)
```

Ahora probaremos a firmar un documento, local en nuestro caso, nos pide que seleccionemos el certificado de nuestro depósito con el cual queremos firmar el documento, et voila..

![](https://valhue.gitlab.io/imagenes/2019/03/certificado-digital.png)

Ya funciona!!

Espero haberos ayudado,  *** ;) ***

