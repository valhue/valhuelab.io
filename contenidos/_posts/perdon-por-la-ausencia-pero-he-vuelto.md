---
title: "Perdón por la ausencia, pero...¡HE VUELTO!"
date: "2014-08-17T18:14:40+01:00"
tags: ["El Blog", "Web"]
categories: ["El Blog"]
---


## ¡He vuelto!

Parecía imposible, ha transcurrido casi un año desde mi última entrada en este blog, no encontraba tiempo para enviar un artículo, pero si os fijáis con detalle, el blog ha ido cambiando. Casi todas las metas que me había marcado en la ultima entrada las he cumplido, pero además:

*   *He cambiado el alojamiento a GitHub*.
*   *El Blog ya dispone de un* ***motor de búsqueda*** *interno*.
*   *Las páginas de errores las personalicé. Al cambiar el alojamiento a GitHub, la pagina de error 404 la he vuelto a regenerar, pero las otras de momento no se como hacer que funcionen con GitHub, pero es que el alojamiento es gratis*  ;)
*   *El sistema de* ***smiles***, *funciona a las mil maravillas*.
*   *Al estar alojado en GitHub tengo un control total sobre las versiones de cada uno de los archivos que componen este Blog*.
*   *Tengo pendientes crear y/o mejorar una* ***página de archivos*** *y una* ***página de descargas***, *que no solo muestre aquellas entradas con la categoría*.<!-- more -->

Iré detallando cada uno de estos ***logros*** en sendas entradas. Por lo que ya tengo trabajo por delante, pero igual ayudo a alguien con ello.

Espero no tardar otro año para mi próxima entrada. 

&nbsp; :D &nbsp;

Os garantizo que no.
