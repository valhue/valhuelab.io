---
title: "Aprende a utilizar el directorio ~/.local"
date: "2010-04-06 16:07"
categories: ["Linux"]
tags: ["Linux", "Informática"]
---

#### &nbsp; ####

Sea cual sea tu distribución de linux, todas utilizan la misma jerarquía de directorios.

Esta entrada puede ser de utilidad para cualquiera que sea tu distribución, pero está optimizada y probada en Ubuntu, posiblemente en otras distribuciones tengas que alterar algún paso, pero seguro que la idea te sirve.

El directorio ~/.local es el equivalente al /usr. Es decir su topología interna es la misma y su funcionalidad también lo es. La diferencia es que /usr es común a todos los usuarios y ~/.local solo para nuestro usuario.

Si nos fijamos en la figura, podemos observar cual es la funcionalidad de cada uno de los directorios que componen un sistema linux.

[![Arbol directorios Linux][1]][2]

-- ¿Si en ~/.local/bin/ creo un ejecutable, podré ejecutarlo sin necesidad de escribir toda su ruta?<!-- more -->

 --¡Exacto!

Esa es su utilidad. Del mismo modo, si en ~/.local/share/applications creo un archivo .desktop este se mostrará en el menú principal de gnome, o si añado un icono en ~/.local/share/icons, este podrá utilizarse por las aplicaciones de ese usuario. Creo que está clara la utilidad de este directorio, ¿no?

Vamos ahora a activar ~/.local/bin para colocar allí nuestros ejecutables o scripts personales.

En primer lugar creamos el archivo .bash\_profile si no lo tenemos:

``` bash
    touch .bash_profile
    nano ~/.bash_profile
```

Y colocamos en su interior lo siguiente:

``` bash
    # $HOME/.bashrc
    # set PATH so it includes user's private bin if it exists
    if [ -d ~/.local/bin ] ; then
        PATH=~/.local/bin:"${PATH}"
    fi
```

Ahora nos aseguraremos que se carga el archivo de perfil (~/.bash\_profile) al iniciar sesión, añadiendo lo siguiente a .bashrc:

``` bash
    nano .bashrc

    # Activamos .bash_profile
    if [ -f ~/.bash_profile ]; then
        . ~/.bash_profile
    fi
```

En el siguiente inicio de sesión ya estará activado el archivo de perfil, y cualquier ejecutable que tengamos en ~/.local/bin se ejecutará con llamarlo sin necesidad de escribir toda su ruta.

Espero que esta entrada os sea de ayuda.

Saludos.

** :D **

[1]: https://valhue.gitlab.io/imagenes/2010/04/fhs-esp-300x243.jpg "Arbol directorios Linux"
[2]: https://valhue.gitlab.io/imagenes/2010/04/fhs-esp.jpg "Arbol directorios Linux"
