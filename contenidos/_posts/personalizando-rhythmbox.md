---
title: "Personalizando Rhythmbox"
date: "2009-11-10T16:50:12+01:00"
categories: ["Linux"]
tags: ["Linux", "Informática", "Karmic", "Lucid", "Ubuntu"]
---

El Reproductor de música que utilizo es Rhythmbox, que además viene por defecto en Gnome, integrándose de maravilla con el sistema. Pero hecho en falta algunos plugins que lo hacen mucho más eficaz y vistoso. Estos son:

![][1]

- Desktop-Art.
- Icecast.
- Radio-Browser.

Para instalarlos existe un repositorio **ppa** que nos facilita la tarea. Instalarlo en **karmic** es tan fácil como:

``` bash
    sudo add-apt-repository ppa:segler-alex
    sudo apt-get update
    sudo apt-get install rhythmbox-desktop-art rhythmbox-icecast rhythmbox-radio-browser
```

[1]: https://valhue.gitlab.io/imagenes/2009/11/Pantallazo.png "Desktop Art en Rhythmbox"
