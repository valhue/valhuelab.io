---
title: "Primer bug de Gnome 3.14"
date: "2014-10-20T11:42:45+01:00"
categories: ["Linux"]
tags: ["ArchLinux", "Gnome", "3.14", "nautilus"]
---

Los usuarios de **ArchLinux** ya tenemos ***gnome 3.14*** entre nosotros. No voy a hacer un análisis del nuevo escritorio porque existen multitud de ellos por internet, y casi todos los blogs dedicados a linux tratan de ello en estos días.

El objeto de esta entrada es la de corregir un *bug* que se auto-instala al actualizarse a la ultima versión de nuestro conocido escritorio.


Para saber si padeces este bug visualiza el contenido de /usr/share/applications/mimeinfo.cache
``` bash
	cat /usr/share/applications/mimeinfo.cache
```
Si en él se encuentra la siguiente linea:
``` bash
	inode/directory=easytag.desktop;org.gnome.Nautilus.desktop;audacious.desktop;
```

seguramente cuando intentes abrir un directorio desde cualquier programa que lo llame, no se abrirá con nautilus si no con easytag si lo tienes instalado, como es mi caso y el de muchos usuarios.<!-- more -->

Entonces tirarás de la [**wiki**](https://wiki.archlinux.org/index.php/Nautilus_%28Espa%C3%B1ol%29#Nautilus_ya_no_es_el_administrador_de_archivos_por_defecto) y aplicarás la receta allí explicada y después de reiniciar sesion, de reiniciar sistema, observarás que no ha servido de nada. El problema no es la receta de la wiki, el problema es que el archivo desktop que lanza nautilus, ya no se llama ***nautilus.desktop***, ahora se llama ***org.gnome.Nautilus.desktop***

### ¿Y ahora que?

Pues aplicamos de nuevo la receta, pero ahora con el nombre correcto del lanzador:
``` bash
	xdg-mime default org.gnome.Nautilus.desktop inode/directory
```
Ahora nuestro archivo *~/.local/share/applications/mimeapps.list* tendra la entrada correcta
``` bash
	[Default Applications]
	inode/directory=org.gnome.Nautilus.desktop
```
y como este archivo de configuración es de nuestro usuario prevalecerá sobre lo que ponga en /usr/share/applications/mimeinfo.cache que llamaba en primer lugar a **easytag** (*easytag.desktop*) solucionando el problema para nuestro usuario.

Si queremos una solución global para todo el sistema bastaría con editar */usr/share/applications/mimeinfo.cache* y después de modificar la linea
``` bash
	inode/directory=easytag.desktop;org.gnome.Nautilus.desktop;audacious.desktop;
```
y dejarla a nuestro gusto, ejecutaríamos:
``` bash
	update-desktop-database
```
Espero que quienes os hayáis encontrado con este pequeño bug lo hayáis podido solucionar con esta entrada.

Que lo disfrutéis. &nbsp; ;) &nbsp;
