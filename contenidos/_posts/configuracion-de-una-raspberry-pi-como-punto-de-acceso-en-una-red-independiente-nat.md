---
title: >-
  Configuración de una Raspberry Pi como punto de acceso en una red
  independiente (NAT)
date: 2019-03-29 08:50:40
categories: RaspBerry PI
tags:
  - RaspBeerry PI
  - Linux
  - Raspbian
---

La Raspberry Pi se puede utilizar como un punto de acceso inalámbrico, ejecutando una red independiente. Esto se puede hacer usando las funciones inalámbricas incorporadas de Raspberry Pi 3 o Raspberry Pi Zero W, o usando un dispositivo adaptador WiFI-USB adecuado que admita puntos de acceso.

{% colorquote warning %}
Tenga en cuenta que esta documentación se probó en una Raspberry Pi 3, y es posible que algunos dongles USB necesiten pequeños cambios en sus configuraciones. Si tiene problemas con una mochila inalámbrica USB, consulte los foros.
{% endcolorquote %}

Ten en cuenta que esta documentación se probó en una Raspberry Pi 3, y es posible que algunos dongles USB necesiten pequeños cambios en sus configuraciones. Si tienes problemas con una mochila inalámbrica USB, consulta los foros de [**raspberrypi.org**](https://www.raspberrypi.org/forums/ "Raspberry Pi Forums")<!-- more -->

Para funcionar como un punto de acceso, la Raspberry Pi deberá tener instalado un software de punto de acceso, junto con el software del servidor DHCP para proporcionar dispositivos de conexión con una dirección de red. Asegúrate de que tu Raspberry Pi está utilizando una versión actualizada de Raspbian (con fecha de 2017 o posterior).

Teclea lo siguiente para actualizar tu RasPI:

```bash
$> sudo apt-get update
$> sudo apt-get upgrade
```

Instala todo el software requerido de una sola vez con este comando:

```bash
$> sudo apt-get install dnsmasq hostapd
```
Como los archivos de configuración, de los nuevos servicios instalados, ***aún no están listos***, asegúrate de desactivarlos de la siguiente manera:

```bash
$> sudo systemctl stop dnsmasq
$> sudo systemctl stop hostapd
```
{% colorquote danger %}
Para asegurarte de que el kernel actualizado esté configurado correctamente después de la instalación, reinicia:
{% endcolorquote %}

```bash
$> sudo reboot
```



#### **Configurando una IP estática.**

Estamos configurando una red independiente para que actúe como un servidor, por lo que la Raspberry Pi debe tener una dirección IP estática asignada al puerto inalámbrico.

{% colorquote info %}
Esta documentación asume que estamos utilizando las direcciones IP estándar 192.168.x.x para nuestra red inalámbrica, por lo que le asignaremos la dirección IP 192.168.4.1 al servidor. También se supone que el dispositivo inalámbrico que se está utilizando es wlan0.
{% endcolorquote %}

Para configurar la dirección IP estática, edita el archivo de configuración dhcpcd con:

```bash
$> sudo nano /etc/dhcpcd.conf
```
Vé al final del archivo y edítalo para que se vea como sigue:

```bash
interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant
```

Ahora reinicia el servicio dhcpcd y activa la nueva configuración de wlan0:

```bash
$> sudo service dhcpcd restart
```



#### **Configurando el servidor DHCP (dnsmasq)**

El servicio DHCP será proporcionado por dnsmasq. De forma predeterminada, el archivo de configuración contiene una gran cantidad de información que no es necesaria, y es más fácil comenzar desde cero. Renombra este archivo de configuración y edita uno nuevo:

```bash
$> sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig  
$> sudo nano /etc/dnsmasq.conf
```

Escribe o copia la siguiente información en el archivo de configuración de dnsmasq y guárdalo:

```bash
interface=wlan0      # Use the require wireless interface - usually wlan0
  dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
```

Entonces, con wlan0, vamos a proporcionar direcciones IP entre 192.168.4.2 y 192.168.4.20, con un tiempo de vida de 24 horas. Si está proporcionando servicios DHCP para otros dispositivos de red (por ejemplo, eth0), podrías agregar más secciones con el encabezado de interfaz adecuado, con el rango de direcciones que sepretende proporcionar a esa interfaz.

Hay muchas más opciones para dnsmasq; Consulta la documentación de dnsmasq para más detalles.



#### **Configurando el software del host del punto de acceso (hostapd)**

Necesitamos editar el archivo de configuración de hostapd, ubicado en ***/etc/hostapd/hostapd.conf***, para agregar los diversos parámetros para la red inalámbrica. Después de la instalación inicial, este será un archivo nuevo/vacío.

```bash
$> sudo nano /etc/hostapd/hostapd.conf
```
Añade la siguiente información al archivo de configuración. Esta configuración asume que estamos usando el canal 7, con un nombre de red de NameOfNetwork y una contraseña AardvarkBadgerHedgehog. Ten en cuenta que el nombre y la contraseña no deben tener comillas a su alrededor. La frase de contraseña debe tener entre 8 y 64 caracteres de longitud.

Para usar la banda de 5 GHz, puedes cambiar el modo de operación de hw_mode=g a hw_mode=a. Los posibles valores para hw_mode son:

- a = IEEE 802.11a (5 GHz)
- b = IEEE 802.11b (2.4 GHz)
- g = IEEE 802.11g (2.4 GHz)
- ad = IEEE 802.11ad (60 GHz)

```bash
interface=wlan0
driver=nl80211
ssid=NameOfNetwork
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=AardvarkBadgerHedgehog
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

Ahora debemos decirle al sistema dónde encontrar este archivo de configuración.

```bash
$> sudo nano /etc/default/hostapd
```

Encuentra la línea con #DAEMON_CONF, y reemplázala con esto:

```bash
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```



#### **Puesta en marcha**

Ahora ponemos en marcha los servicios restantes.

```bash
$> sudo systemctl start hostapd
$> sudo systemctl start dnsmasq
```

##### **Añadimos el enrutamiento y la mascara**

Edita ***/etc/sysctl.conf*** y elimina el comentario de esta línea:

```bash
net.ipv4.ip_forward=1
```
Agregamos una mascara para el tráfico saliente en eth0:

```bash
$> sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
```

Guardamos la regla de iptables.

```bash
$> sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
```

Editamos /etc/rc.local y agregamos esto justo encima de "exit 0" para instalar estas reglas en el arranque.

```bash
iptables-restore < /etc/iptables.ipv4.nat
```
Reiniciamos

Usando un dispositivo inalámbrico, busca redes. El SSID de la red que especificamos en la configuración de hostapd ahora debería ser visible, y se debería acceder con la contraseña especificada.

Si SSH está habilitado en el punto de acceso de Raspberry Pi, debería ser posible conectarse a él desde otra consola de Linux (o un sistema con conectividad SSH presente) de la siguiente manera, asumiendo que la cuenta pi está presente.

```bash
$> ssh pi@192.168.4.1
```

Llegados a este punto, la Raspberry Pi está actuando como un punto de acceso, y otros dispositivos pueden asociarse con él. Los dispositivos asociados pueden acceder al punto de acceso de la Raspberry Pi a través de su dirección IP para operaciones como rsync, scp o ssh.

Disfrutarlo ** ;) **