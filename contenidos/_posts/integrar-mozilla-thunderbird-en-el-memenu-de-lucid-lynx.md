---
title: Integrar Mozilla ThunderBird en el "MeMenu" de Lucid Lynx
date: "2010-05-09 21:27"
categories: ["Linux"]
tags: ["Linux", "Informática", "Karmic", "Lucid", "MeMenu", "Thunderbird", "Ubuntu"]
---

###[actualizado 11-9-2010]###

Al igual que comentaba en la entrada anterior sobre como "Integrar Liferea en el "MeMenu" de Lucid Lynx", se puede integrar el cliente de correo Mozilla Thunderbird (es el que yo utilizo).

En primer lugar crearemos el enlace al cliente de correo en el Indicator-Applet, tal y como comentamos en la entrada anterior:

Creamos un archivo en /usr/share/indicators/messages/applications/ llamado thunderbird, en cuyo interior escribiremos

``` bash
    /usr/share/applications/thunderbird.desktop
```

Con eso me aparece el menú de thunderbird en el indicator-applet del MeMenu. Para los menos iniciados:<!-- more -->

``` bash
    sudo touch /usr/share/indicators/messages/applications/thunderbird
    sudo nano /usr/share/indicators/messages/applications/thunderbird
```

y dentro de este

``` bash
    /usr/share/applications/thunderbird.desktop
```

Con esto habremos cumplido con la mitad de nuestro propósito, ya podemos arrancar TB desde allí, pero ¿**como conseguimos que nos avise de los correos nuevos que recibimos**?

Para ello instalamos el siguiente complemento, e instalaremos una dependencia de este.

``` bash
    sudo aptitude install python-indicate
```

[libnotify-mozilla-0.2.1.xpi][1]

y después de reiniciar TB (ThunderBird), ajustamos las preferencias tal y como muestra la imagen, en caso de utilizar TB para leer newsgroups (grupos de debate en usenet) marcaríamos la casilla de la ultima opción.

![Preferencias del complemento][2]

Si además modificamos el archivo ***/usr/share/applications/thunderbird.desktop*** y añadimos en su interior:

``` bash
    X-Ayatana-Desktop-Shortcuts=Compose;Contacts

    [Compose Shortcut Group]
    Name=Redactar un mensaje nuevo
    Exec=thunderbird -compose
    OnlyShowIn=Messaging Menu

    [Contacts Shortcut Group]
    Name=Libreta de direcciones
    Exec=thunderbird -addressbook
    OnlyShowIn=Messaging Menu
```

obtendremos en el MeMenu las entradas para ver nuestros contactos o redactar un nuevo correo, al igual que ocurre con el cliente evolution, la única pega de hacer esto es que con cada actualización de thunderbird este archivo nos lo modificará y tendremos que volver a añadir a su contenido el párrafo anterior.

![Thunderbird MeMenu][3]

¡¡ Que lo disfrutéis !!

** :D **

[1]: http://launchpad.net/libnotify-mozilla/0.2/0.2.1/+download/libnotify-mozilla-0.2.1.xpi "libnotify-mozilla-0.2.1.xpi"
[2]: https://valhue.gitlab.io/imagenes/2010/05/Preferences_001.png "Preferencias del complemento"
[3]: https://valhue.gitlab.io/imagenes/2010/05/Thunderbird_MeMenu_001.png "Thunderbird MeMenu"
