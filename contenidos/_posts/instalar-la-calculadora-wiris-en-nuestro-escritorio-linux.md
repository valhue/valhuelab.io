---
title: "Instalar la calculadora Wiris en nuestro escritorio linux"
date: "2010-09-18T11:37:44+01:00"
categories: ["Linux"]
tags: ["Científica", "Linux", "Informática", "Lucid", "Matemáticas", "Ubuntu"]
---


La [WIRIS Desktop][1] es la versión de escritorio de la WIRIS CAS, calculadora científica on-line utilizada por gran numero de centros educativos a distancia, sin necesidad de tener conexión a internet.

[![][2]][10]

Podeis saber más visitando estos enlaces:<!-- more -->

-  [**Características**][3].
-  [**Vídeos**][4].

Para instalarla, en mi caso en Ubuntu Lucid, nos registramos en la zona de descargas de WIRIS Desktop, y obtendremos así una licencia válida para 60 días. Una vez transcurridos tendremos que comprar una licencia. Pero... existen multitud de [organismos públicos][5] que regalan las licencias; por ejemplo, la [UOC][6] a todos sus estudiantes, [el departamento de Educación de la Generalitat de Catalunya][7], etc...

Nos descargamos la versión demo una vez registrados, bien desde la misma página, o bien con:
``` bash
    wget -O WirisDesktop.zip http://www.wiris.com/es/downloads/files/1351/WIRISdesktop-linux-2.3.4.0371.zip
```
extraemos el contenido del zip con:
``` bash
    unzip WirisDesktop-linux.zip
```
y ahora dependiendo si la queremos instalar para todos los usuarios o solo en nuestra cuenta haremos:
``` bash
    mv WirisDesktop ~/.local/lib/ (para nuestro usuario)
    sudo mv WirisDesktop /usr/local/share/ (para todos los usuarios)
```
Yo lo he instalado en mi cuenta (mono-usuario), pero el resto es común para el caso "multi-usuario" solo que tendréis que tener cuidado con las rutas.

Ahora añado el ejecutable a ~/.local/bin/ que es donde tiene mi usuario sus binarios:
``` bash
    ln -s ~/.local/lib/WirisDesktop/WirisDesktop ~/.local/bin/WirisDesktop
```
Editamos el binario para corregir la ruta a las clases de java:
``` bash
    gedit ~/.local/lib/WirisDesktop/WirisDesktop
```
y cambiamos
``` bash
    binPath=`dirname "$cmdHere"`
```
por
``` bash
    binPath="$HOME/.local/lib/WirisDesktop"
```
Si ejecutamos WirisDesktop desde una terminal ya debería iniciarse, pero si observamos problemas con java (es necesario tener java instalado), lo mejor es eliminar el directorio java de WirisDesktop, pues lleva su propia versión y esta podría no ser compatible del todo con nuestro sistema.
``` bash
    rm -rf ~/.local/lib/WirisDesktop/java
```
y editamos el archivo ejecutable WirisDesktop para que utilice el java de nuestro sistema. Primero averiguamos la ruta:
``` bash
    type java
```
anotamos el resultado, en mi caso /usr/bin/java, y cambiamos la linea
``` bash
    wd_java=./java/linux/java/bin/java
```
por
``` bash
    wd_java=/usr/bin/java
```
ahora funcionará con el java del sistema. Elegimos el idioma en la primer pantalla al iniciar WirisDesktop,

![Selección de idioma - WIRIS DESKTOP][8]

y a continuación la licencia que  nos han enviado por correo al registrarnos. Recordad que la licencia es valida por 60 días.

[![Clave de producto requerida - WIRIS Desktop][9]][11]

Ahora crearemos un lanzador en el menú de Gnome.

En primer lugar enlazamos la imagen que utilizaremos como icono al lugar
adecuado,
``` bash
    ln -s ~/.local/lib/WirisDesktop/desktop.png ~/.local/share/icons/wirisdesktop.png
```
y ahora creamos el archivo wirisdesktop.desktop
``` bash
    touch ~/.local/share/applications/wirisdesktop.desktop
```
lo editamos y pegamos en su interior lo siguiente:
``` bash
    #!/usr/bin/env xdg-open

    [Desktop Entry]
     Version=1.0
     Encoding=UTF-8
     Type=Application
     Name=WIRIS Desktop
     GenericName=WIRIS Desktop
     Comment=WIRIS Desktop
     Icon=wirisdesktop
     Exec=~/.local/lib/WirisDesktop/WirisDesktop
     Terminal=false
     Categories=Education
```
ya la tenemos en el menú en **Aplicaciones -> Educación -> WIRIS DeskTop** y podemos disfrutar de ella.

¡ Que lo disfrutéis ! y sacadle provecho.

&nbsp; ;) &nbsp;

[1]: http://www.wiris.com/es/cas/desktop/docs/ "WIRIS Desktop"
[2]: https://valhue.gitlab.io/imagenes/2010/09/WIRIS-Desktop-2-examples-es-04_analysis.wiris_001-300x225.png "WIRIS Desktop 2"
[3]: http://www.wiris.com/es/cas/features/ "Características"
[4]: http://www.wiris.com/es/cas/videos/ "Vídeos"
[5]: http://www.wiris.com/component/option,com_frontpage/Itemid,1/lang,es/ "Organismos públicos que dan licencia"
[6]: http://www.uoc.edu "UOC"
[7]: http://www.xtec.cat "Departamento de Educación de la Generalitat de Catalunya"
[8]: https://valhue.gitlab.io/imagenes/2010/09/wiris_idioma.png "Selección de idioma - WIRIS DESKTOP"
[9]: https://valhue.gitlab.io/imagenes/2010/09/WIRIS-Desktop-2-Clave-de-producto-requerida_001-300x213.png "Clave de producto requerida - WIRIS Desktop"
[10]: https://valhue.gitlab.io/imagenes/2010/09/WIRIS-Desktop-2-examples-es-04_analysis.wiris_001.png "WIRIS Desktop 2"
[11]: https://valhue.gitlab.io/imagenes/2010/09/WIRIS-Desktop-2-Clave-de-producto-requerida_001.png "Clave de producto requerida - WIRIS Desktop"
