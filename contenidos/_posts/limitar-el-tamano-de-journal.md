---
title: "Limitar el tamaño de journal"
date: "2013-01-12T14:47:29+01:00"
categories: ["Linux"]
tags: ["Linux", "ArchLinux", "Journal", "Systemd"]
---

### &nbsp; ###

Bien, hoy me he dado "***un susto de narices***", resulta que Arch utiliza [systemd][1] desde hace unas cuantas isos, y yo migré en su día a systemd siguiendo un interesante [manual][2] elaborado por [Gregorio Espadas][3], y ***hoy me aparece un mensaje*** diciéndome que me estoy quedando ***sin espacio en la partición /***.

Después del oportuno tiempo para digerir tal noticia, me he puesto a pensar (lo hago de vez en cuando :D ), y he consultado el espacio ocupado por el journal de systemd.
``` bash
    sudo journalctl --disk-usage
```
Tenía 3GB de tamaño el dichoso journal!!, ¿y porqué?.<!-- more -->

Pues bien, cuando se crea el journal, se va añadiendo a él todos los registros de cada sesión, sin borrar el anterior, de no ser que lo tengamos en una partición volátil  el cual no es mi caso y supongo que tampoco lo és para la mayoría de usuarios. Si venimos de una instalación anterior al uso de systemd, debemos crear el directorio /var/log/journal, que será donde por defecto se escriba nuestro journal. Mirando en la wiki de Arch, he encontrado que si no tiene acceso a ese directorio el journal lo escribe en /run/systemd/journal por lo que he mirado allí también por si tenia otra sorpresa, pero bien, no tengo nada de que preocuparme.

El caso es que podemos limitar el tamaño de nuestro journal fácilmente y así no llegará a ocupar el espacio libre que tuviéramos en nuestra partición raíz, y es muy fácil. Editamos el archivo /etc/systemd/journald.conf y hacemos algunos ajustes
``` bash
    sudo nano /etc/systemd/journald.conf
```
descomentamos y cambiamos las siguientes entradas
``` bash
    Storage=auto
    SystemMaxUse=50M
```
* ***Storage=auto*** le indica dónde queremos que guarde el journal, la dirección por defecto es /var/log/journal.
* ***SystemMaxUse=50M*** le indica que el tamaño máximo de nuestro journal será de 50Mb.

Borramos el journal existente con
``` bash
    sudo rm -rf /var/log/journal/
```
Reiniciamos y observamos el nuevo espacio que ocupa con
``` bash
    sudo journalctl --disk-usage
```
Como acabamos de reiniciar podremos observar que ahora solo tiene unos cuantos Mbs de espacio ocupado. Para ajustar aun mas nuestro journal tenemos la información sobre las demás entradas de
/etc/systemd/journald.conf con
``` bash
    man journald.conf
```
Espero que esta información os sea de ayuda a tod@s l@s Archers.

Un Saludo.

[1]: http://0pointer.de/blog/projects/why.html "Why systemd?"
[2]: http://gespadas.com/archlinux-systemd "Migrando a systemd en ArchLinux"
[3]: http://gespadas.com "Web de Gregorio Espadas"
