---
title: "Nueva etapa"
date: "2018-10-09T22:44:45+01:00"
categories: ["El Blog"]
tags: ["El Blog", "Web"]
---


Saludos a tod@s.  *** :D ***

​	Después de  algún tiempo (algunos años *** :[ ***) sin escribir absolutamente nada en el blog, vuelvo con la intención de no volver a permanecer tanto tiempo inactivo. Aprovechando las circunstancias también apreciareis algunos cambios:

- **Cambio de servidor**. Ahora tengo todo el código del blog en GitLab. GitHub para quien no lo sepa ahora es propiedad de MicroSoft, y...

  ​	Además en GitLab no tengo que generar el código html y subirlo a una rama vinculada con el repositorio publico, tal y como se asignaba en GitHub con la rama master y sus GitHub-Pages.

  <!-- more -->

  Más adelante publicaré una entrada de cómo generar un blog o una pagina web en GitLab.

- **Cambio de framework**. Ya sabéis que soy fan de usar código HTML y añadirle un servicio de comentarios como Disqus, indexar el contenido y utilizar algún motor de búsqueda externo, etc..

  ​	De esa manera la web es estática y con un simple alojamiento tengo suficiente, y posiblemente mucho más seguro que utilizar bases de datos. Empece utilizando Pelican que me entregaba el contenido estático con un simple comando, más adelante descubrí Hugo y me convenció más, por lo que migre de Pelican a Hugo.

  ​	Ahora utilizo Hexo. Para mi es maravilloso. Cumple con todas las necesidades que tengo para mantener el blog, y además GitLab lo tiene implementado, por lo que unicamente he crear el contenido en texto plano (utilizando archivos con formato markdown) y el propio GitLab con su sistema de pipelines me genera la parte publica que es el contenido que vosotros veis.

  ​	¿No es genial? *** ;) ***

- **Cambio de tema**. Ahora he instalado un tema mucho más simple y práctico (eso creo *** :S ***).

- **Recuperación de mensajes antiguos**. He trasladado todas las entradas de GitHub a Gitlab. Cómo antes utilizaba Hugo, las entradas ya estaban en formato md (markdown) y no ha sido necesario hacer casi nada.



​	Con todo esto el blog vuelve a estar operativo. Espero que encontréis lo que buscabais, así que poneos comod@s y a disfrutar.

Saludos a tod@s.  *** :D ***
